@extends('layouts.app')

@section('title', 'Lend Movie')

@section('content')
	<h3> Lend Movie </h3>
	<br>
	<form method="post" action="{{ route('lend.store') }}">
		@csrf
	  <div class="form-group">
	    <label for="movie-title">Movie</label>
	    <select class="form-control" id="movie-title" name="movie_id">
	      <option>Select Movie</option>
	      @foreach($movies as $movie)
			<option value="{{ $movie->id }}" {{ $movie->id == old('movie_id') ? 'selected' : '' }}>{{ $movie->title }}</option>
	      @endforeach
	    </select>
	  </div>
	  <div class="form-group">
	    <label for="member-name">Member</label>
	    <select class="form-control" id="member-name" name="member_id">
	      <option>Select Member</option>
	      @foreach($members as $member)
			<option value="{{ $member->id }}" {{ $member->id == old('member_id') ? 'selected' : '' }}>{{ $member->name }}</option>
	      @endforeach
	    </select>
	  </div>
	  <div class="form-group">
	    <label for="lend-duration">Lend Duration</label>
	    <select class="form-control" id="lend-duration" name="duration">
	      <option>Select Duration</option>
	      <option value="7" {{ 7 == old('duration') ? 'selected' : '' }}>1 Week</option>
	      <option value="14" {{ 14 == old('duration') ? 'selected' : '' }}>2 Weeks</option>
	      <option value="24" {{ 24 == old('duration') ? 'selected' : '' }}>3 Weeks</option>
	      <option value="36" {{ 36 == old('duration') ? 'selected' : '' }}>4 Weeks</option>
	    </select>
	  </div>
	  <button type="submit" class="btn btn-success float-right">Save</button>
	  <button type="reset" class="btn btn-default float-right">Reset</button>
	</form>
@endsection

@section('extra-js')
	<script type="text/javascript">
		@if($errors->any())
	    	swal({
	    		title: 'Failed!',
			    text: 'Oops! There is Something Wrong',
			    icon: 'error'
	    	});
		@endif

		@if(session('success') == 'store')
		    	swal({
		    		title: 'Success!',
				    text: 'Your data has been stored.',
				    icon: 'success'
		    	});
		    @endif
	</script>
@endsection