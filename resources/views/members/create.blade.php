@extends('layouts.app')

@section('title', 'Add Movies')

@section('content')
	<h3> Add New Member </h3>
	<br>
	<form method="post" action="{{ route('member.store') }}">
		@csrf
	  <div class="form-group">
	    <label for="member-name">Name</label>
	    <input type="text" class="form-control" name="name" id="member-name" placeholder="Name" value="{{ old('name') }}">
	  </div>
	  <div class="form-group">
	    <label for="member-age">Age</label>
	    <input type="number" min="0" class="form-control" name="age" id="member-age" placeholder="Age" value="{{ old('age') }}">
	  </div>
	  <div class="form-group">
	    <label for="member-address">Address</label>
	    <textarea class="form-control" id="member-address" name="address">{{ old('address') }}</textarea>
	  </div>
	  <div class="form-group">
	    <label for="member-telephone">Telephone</label>
	    <input type="number" min="0" class="form-control" name="telephone" id="member-telephone" placeholder="Telephone" value="{{ old('telephone') }}">
	  </div>
	  <div class="form-group">
	    <label for="member-identity_number">Identity Number</label>
	    <input type="number" min="0" class="form-control" name="identity_number" id="member-identity_number" placeholder="Identity Number" value="{{ old('identity_number') }}">
	  </div>
		<div class="form-group">
	    <label for="member-joined_date">Joined Date</label>
	    <input type="date" class="form-control" name="joined_date" id="member-joined_date" placeholder="" value="{{ old('joined_date') }}">
	  </div>
	  <input type="hidden" name="is_active" value="1">
	  <button type="submit" class="btn btn-success float-right">Save</button>
	  <button type="reset" class="btn btn-default float-right">Reset</button>
	</form>
@endsection

@section('extra-js')
	<script type="text/javascript">
		@if($errors->any())
	    	swal({
	    		title: 'Failed!',
			    text: 'Oops! There is Something Wrong',
			    icon: 'error'
	    	});
		@endif
	</script>
@endsection