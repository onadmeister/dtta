@extends('layouts.app')

@section('title', 'Movies')

@section('content')
		<table id="movie-table" class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<td>#</td>
					<td>Movie Title</td>
					<td>Borrowed By</td>
					<td>Return Date</td>
					<td>Lateness Fee</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>
				@foreach($data as $datum)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td>{{ $datum->movie->title }}</td>
						<td>{{ $datum->member->name }}</td>
						<td>{{ $datum->expected_returned_date->format('Y-m-d') }}</td>
						@if(\Carbon\Carbon::now() <= $datum->expected_returned_date)
							<td>0</td>
						@else
							<td>{{ \Carbon\Carbon::now()->diffInDays($datum->expected_returned_date) * $datum->lateness_charge_per_day }}</td>
						@endif
						<td><a href="#" class="movie-data" data-id="{{ $datum->id }}">Return</a></td>
					</tr>
				@endforeach
			</tbody>
		</table>

		<form method="post" id="delete-movie" action="">
			@csrf
			{{ method_field('patch') }}
		</form>
@endsection

@section('extra-js')
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#movie-table').DataTable();

		    @if(session('success') == 'store')
		    	swal({
		    		title: 'Success!',
				    text: 'Movie has been returned successfully',
				    icon: 'success'
		    	});
		    @endif

		    $(".movie-data").on('click', function() {
		    	swal({
				  title: "Are you sure?",
				  text: "Are You Sure You Wanna Return This Movie?",
				  icon: "warning",
				  buttons: true,
				  dangerMode: true,
				})
				.then((willDelete) => {
				  if (willDelete) {
				  	var $id = $(this).data('id');
				    $("#delete-movie").attr('action', "returnment/" + $id);
				    $("#delete-movie").submit();
				  } else {

				  }
				});
		    });
		} );
	</script>
@endsection