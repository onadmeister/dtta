@extends('layouts.app')

@section('title', 'Edit Movies')

@section('content')
	<h3> Edit Movie: {{ $movie->title }} </h3>
	<br>
	<form method="post" action="{{ route('movie.update', ["id" => $movie->id]) }}">
		@csrf
		{{ method_field('patch') }}
	  <div class="form-group">
	    <label for="movie-title">Title</label>
	    <input type="text" class="form-control" name="title" id="movie-title" placeholder="Title" value="{{ $movie->title }}">
	  </div>
	  <div class="form-group">
	    <label for="movie-genre">Genre</label>
	    <input type="text" class="form-control" name="genre" id="movie-genre" placeholder="Genre" value="{{ $movie->genre }}">
	  </div>
	  <div class="form-group">
	    <label for="movie-releasedate">Released Date</label>
	    <input type="date" class="form-control" name="released_date" id="movie-releasedate" placeholder="" value="{{ $movie->released_date->format('Y-m-d') }}">
	  </div>
	  <button type="submit" class="btn btn-success float-right">Save</button>
	  <button type="reset" class="btn btn-default float-right">Reset</button>
	</form>
@endsection

@section('extra-js')
	<script type="text/javascript">
		@if($errors->any())
	    	swal({
	    		title: 'Failed!',
			    text: 'Oops! There is Something Wrong',
			    icon: 'error'
	    	});
		@endif
	</script>
@endsection