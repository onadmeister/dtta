@extends('layouts.app')

@section('title', 'Movies')

@section('content')
	<div class="container">
		<h3> Movie Details</h3>

		<div class="row">
			<div class="col-md-2">
				Title:
			</div>
			<div class="col-md-10">
				{{ $movie->title }}
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				Genre:
			</div>
			<div class="col-md-10">
				{{ $movie->genre }}
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				Released Date:
			</div>
			<div class="col-md-10">
				{{ $movie->released_date->format('d F Y') }}
			</div>
		</div>
	</div>
@endsection