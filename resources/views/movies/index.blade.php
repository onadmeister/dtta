@extends('layouts.app')

@section('title', 'Movies')

@section('content')
	<a href="{{ route('movie.create') }}" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i> Add New Movie</a>
		<table id="movie-table" class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<td>#</td>
					<td>Title</td>
					<td>Genre</td>
					<td>Released Date</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>
				@foreach($movies as $movie)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td><a href="{{ route('movie.show', ['id' => $movie->id]) }}">{{ $movie->title }}</a></td>
						<td>{{ $movie->genre }}</td>
						<td>{{ $movie->released_date->format('d M, Y') }}</td>
						<td>
							<a href="{{ route('movie.edit', ['id' => $movie->id]) }}"><i class="fa fa-edit"></i> Edit</a>
							|
							<a href="#" class="movie-data" data-id="{{ $movie->id }}"><i class="fa fa-trash"></i> Delete</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		<form method="post" id="delete-movie" action="">
			@csrf
			{{ method_field('delete') }}
		</form>
@endsection

@section('extra-js')
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#movie-table').DataTable();

		    @if(session('success') == 'store')
		    	swal({
		    		title: 'Success!',
				    text: 'Your data has been stored.',
				    icon: 'success'
		    	});
		    @endif

		    @if(session('success') == 'update')
		    	swal({
		    		title: 'Success!',
				    text: 'Your data has been updated.',
				    icon: 'success'
		    	});
		    @endif

		    @if(session('success') == 'delete')
		    	swal({
		    		title: 'Success!',
				    text: 'Your data has been deleted.',
				    icon: 'success'
		    	});
		    @endif

		    $(".movie-data").on('click', function() {
		    	swal({
				  title: "Are you sure?",
				  text: "Once deleted, you will not be able to recover this data!",
				  icon: "warning",
				  buttons: true,
				  dangerMode: true,
				})
				.then((willDelete) => {
				  if (willDelete) {
				  	var $id = $(this).data('id');
				    $("#delete-movie").attr('action', "movies/" + $id);
				    $("#delete-movie").submit();
				  } else {

				  }
				});
		    });
		} );
	</script>
@endsection