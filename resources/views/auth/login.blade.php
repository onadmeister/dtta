@extends('layouts.app')

@section('title', 'Login')

@section('content')
	<div class="container">
		<div class="row justify-content-md-center">
			<form class="form-signin col-md-6" method="post" action="{{ route('login') }}">
			  @csrf
		      <div class="form-label-group">
		        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required autofocus>
		      </div>
				<br>
		      <div class="form-label-group">
		        <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
		      </div>
				<br>
		      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
		    </form>
		</div>
	</div>

@endsection