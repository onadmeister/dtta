<?php

namespace App\Repositories;

use App\Member;

class MemberRepository
{
    protected $model;

    public function __construct(Member $member)
    {
    	$this->model = $member;
    }

    public function findAll()
    {
    	return $this->model->all();
    }

    public function findOne($id)
    {
        return $this->model->findOrFail($id);
    }

    public function save($request)
    {
    	return $this->model->create($request);
    }

    public function update($request, $id)
    {
        return $this->model->findOrFail($id)->update($request);
    }

    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }

    public function findActive()
    {
        return $this->model->where('is_active', 1)->get();
    }

    public function change_status($id)
    {
        $data = $this->model->findOrFail($id);

        if($data->is_active == 1) {
            return $data->update([
                'is_active' => 0
            ]);
        } else {
            return $data->update([
                'is_active' => 1
            ]);
        }
    }
}
