<?php

namespace App\Http\Controllers\Returnment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\LendingRepository;
use Validator;

class ReturnMovie extends Controller
{
	public function __invoke(LendingRepository $repo, $id) {
		$repo->return_lending($id);

        return redirect()->route('returnment')->with('success', 'store');
	}
}