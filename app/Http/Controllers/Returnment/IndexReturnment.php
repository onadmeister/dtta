<?php

namespace App\Http\Controllers\Returnment;

use App\Http\Controllers\Controller;
use App\Repositories\LendingRepository;

class IndexReturnment extends Controller
{
	public function __invoke(LendingRepository $repo) {
		$data = $repo->showAllUnreturned();

		return view('returnment.index', compact('data'));
	}
}