<?php

namespace App\Http\Controllers\Movies;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MovieRepository;
use Validator;

class UpdateMovie
{
	public function __invoke(Request $request, MovieRepository $repo, $id) {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'genre' => 'required',
            'released_date' => 'required|date|date_format:Y-m-d'
        ]);

        if ($validator->fails()) {
            return redirect()->route('movie.edit', ['id' => $id])
                        ->withErrors($validator)
                        ->withInput();
        }

        $repo->edit($id, $request->all());

        return redirect()->route('movie')->with('success', 'update');
    }
}