<?php

namespace App\Http\Controllers\Movies;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MovieRepository;
use Validator;

class StoreMovie extends Controller
{
    public function __invoke(Request $request, MovieRepository $repo) {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'genre' => 'required',
            'released_date' => 'required|date|date_format:Y-m-d'
        ]);

        if ($validator->fails()) {
            return redirect('movies/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $repo->save($request->all());

        return redirect()->route('movie')->with('success', 'store');
    }
}