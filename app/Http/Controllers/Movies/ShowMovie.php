<?php

namespace App\Http\Controllers\Movies;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MovieRepository;
use Validator;

class ShowMovie extends Controller
{
	public function __invoke(MovieRepository $repo, $id) {
		$movie = $repo->find($id);

		return view('movies.show', compact('movie'));
	}
}