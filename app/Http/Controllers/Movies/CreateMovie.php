<?php

namespace App\Http\Controllers\Movies;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MovieRepository;
use Validator;

class CreateMovie extends Controller
{
	public function __invoke(MovieRepository $repo) {
		return view('movies.create');
    }
}