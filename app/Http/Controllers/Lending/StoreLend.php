<?php

namespace App\Http\Controllers\Lending;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\LendingRepository;
use Validator;
use Carbon\Carbon;

class StoreLend extends Controller
{
	public function __invoke(LendingRepository $lend, Request $request) {
		$validator = Validator::make($request->all(), [
            'movie_id' => 'required',
            'member_id' => 'required',
            'duration' => 'required|numeric',
        ]);

        $request->request->add(
        	[
        		"lending_date" => Carbon::now(),
        		"expected_returned_date" => Carbon::now()->addDays($request->duration),
        		"lateness_charge_per_day" => 1000
        	]
        );

        if ($validator->fails()) {
            return redirect('lend')
                        ->withErrors($validator)
                        ->withInput();
        }

        $lend->save($request->all());

		return redirect()->route('lend')->with('success', 'store');
	}
}