<?php

namespace App\Http\Controllers\Lending;

use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;
use App\Repositories\MovieRepository;

class CreateLend extends Controller
{
	public function __invoke(MovieRepository $movie, MemberRepository $member) {
		$movies = $movie->showAll();
		$members = $member->findActive();

		return view('lending.create', compact('movies', 'members'));
	}
}