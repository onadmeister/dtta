<?php

namespace App\Http\Controllers\Members;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;
use Validator;

class UpdateMember extends Controller
{
	public function __invoke(Request $request, MemberRepository $repo, $id) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'age' => 'required|numeric',
            'address' => 'required',
            'telephone' => 'required|numeric',
            'identity_number' => 'required|numeric',
            'joined_date' => 'required|date|date_format:Y-m-d',
            'is_active' => 'required|boolean'
        ]);

        if ($validator->fails()) {
            return redirect('members/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $repo->update($request->all(), $id);

        return redirect()->route('member')->with('success', 'update');
    }
}