<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;

class IndexMember extends Controller
{
	public function __invoke(MemberRepository $repo) {
		$members = $repo->findAll();

		return view('members.index', compact('members'));
	}
}