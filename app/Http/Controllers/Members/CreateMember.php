<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;

class CreateMember extends Controller
{
	public function __invoke() {
		return view('members.create');
	}
}