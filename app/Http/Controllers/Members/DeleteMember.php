<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;

class DeleteMember extends Controller
{
	public function __invoke(MemberRepository $repo, $id) {
        $repo->delete($id);
        return redirect()->route('member')->with('success', 'delete');
    }
}