<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

/*
|--------------------------------------------------------------------------
| Authentication Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/login', 'HomeController@index')->name('login'); // This will be replaced with Login Page Later
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');



Route::group(['middleware' => 'auth'], function () {
    /*
    |--------------------------------------------------------------------------
    | Movies Routes
    |--------------------------------------------------------------------------
    |
    */
    Route::get('movies', 'Movies\IndexMovie')->name('movie');
    Route::get('movies/create', 'Movies\CreateMovie')->name('movie.create');
    Route::post('movies', 'Movies\StoreMovie')->name('movie.store');
    Route::get('movies/{id}', 'Movies\ShowMovie')->name('movie.show');
    Route::get('movies/{id}/edit', 'Movies\EditMovie')->name('movie.edit');
    Route::patch('movies/{id}', 'Movies\UpdateMovie')->name('movie.update');
    Route::delete('movies/{id}', 'Movies\DeleteMovie');

     /*
    |--------------------------------------------------------------------------
    | Member Routes
    |--------------------------------------------------------------------------
    |
    */
    Route::get('members', 'Members\IndexMember')->name('member');
    Route::get('members/create', 'Members\CreateMember')->name('member.create');
    Route::post('members', 'Members\StoreMember')->name('member.store');
    Route::get('members/{id}/edit', 'Members\EditMember')->name('member.edit');
    Route::patch('members/{id}', 'Members\UpdateMember')->name('member.update');
    Route::delete('members/{id}', 'Members\DeleteMember');
    Route::put('members/{id}', 'Members\ChangeStatusMember');

    /*
    |--------------------------------------------------------------------------
    | Lending Routes
    |--------------------------------------------------------------------------
    |
    */
    Route::get('lend', 'Lending\CreateLend')->name('lend');
    Route::post('lend', 'Lending\StoreLend')->name('lend.store');

    Route::get('returnment', 'Returnment\IndexReturnment')->name('returnment');
    Route::patch('returnment/{id}', 'Returnment\ReturnMovie');
});

