<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Member::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'age' => $faker->randomDigit,
        'address' => $faker->address,
        'telephone' => $faker->phoneNumber,
        'identity_number' => $faker->creditCardNumber ,
        'joined_date' => Carbon::now(),
        'is_active' => 1
    ];
});
