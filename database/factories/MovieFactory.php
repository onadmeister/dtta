<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'genre' => $faker->word,
        'released_date' => Carbon::now()
    ];
});
